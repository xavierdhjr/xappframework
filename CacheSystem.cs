﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAppFramework
{
    public interface ICacheData
    {
        void InvalidateCache(int cacheId);
    }

    public static class CacheType
    {
        public const int TABLE_DATA = 0;
    }

    public class CacheSystem
    {
        static CacheSystem system
        {
            get
            {
                if (_system == null)
                {
                    _system = new CacheSystem();
                }

                return _system;
            }
        }

        static CacheSystem _system;

        List<ICacheData> _caches;

        public CacheSystem()
        {
            _caches = new List<ICacheData>();
        }

        public static void Invalidate(int cacheId)
        {
            List<ICacheData> caches = system._caches;
            for (int i = 0; i < caches.Count; ++i)
            {
                if (caches[i] != null)
                {
                    caches[i].InvalidateCache(cacheId);
                }
                else
                {
                    caches.RemoveAt(i);
                    i--;
                }
            }

        }

        public static void Register(ICacheData cache)
        {
            XApp.ASSERT(() => !system._caches.Contains(cache), XAppFailure.CACHE_ERROR);

            system._caches.Add(cache);
        }

#if DT_TESTING
    public static bool IsRegistered(ICacheData cache)
    {
        return system._caches.Contains(cache);
    }

    public static void SetNull(ICacheData cache)
    {
        int idx =  system._caches.FindIndex(x => x == cache);
        system._caches[idx] = null;

    }
#endif
    }
}