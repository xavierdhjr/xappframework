﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace XAppFramework
{
    /// <summary>
    /// Singletons should implement this interface so they can be initialized at a later time, incase their initialization has bindings
    /// </summary>
    public interface IIoCSingleton
    {
        void InitSingleton();
    }

    public interface IIoCBinder
    {
        void BindAllIoCClasses();
    }

    public class IoCBindings
    {
        class BindingAttributes
        {
            public bool IsSingleton;
            public object SingletonObject;
            public Dictionary<Type, Type> UserMappings = new Dictionary<Type, Type>();
        }

        Dictionary<Type, BindingAttributes> _attributes = new Dictionary<Type, BindingAttributes>();

        public IoCBindings() { }

        public void SetBinding<T_INTERFACE, T_CONCRETE>()
            where T_CONCRETE : T_INTERFACE
        {
            System.Type concrete_type = typeof(T_CONCRETE);
            System.Type interface_type = typeof(T_INTERFACE);

            XApp.ASSERT(() => interface_type.IsInterface, XAppFailure.IOC_BINDING_FAILURE, "The bound type was not an interface: " + interface_type.ToString());
            XApp.ASSERT(() => !concrete_type.IsInterface && !concrete_type.IsAbstract, XAppFailure.IOC_BINDING_FAILURE, "Cannot bind type " + concrete_type.ToString() + " because it's not a concrete type");

            List<System.Type> types = new List<Type>();
            types.Add(concrete_type);

            BindingAttributes attrs = new BindingAttributes()
                {
                    IsSingleton = false,
                    UserMappings = new Dictionary<Type, Type>()
                    {
                        { interface_type, concrete_type }
                    }
                };

            if (_attributes.ContainsKey(interface_type))
            {
                _attributes[interface_type] = attrs;
            }
            else
            {
                _attributes.Add(interface_type, attrs);
            }
        }

        public void SetBinding<T_INTERFACE, T_CONCRETE, T_USER>()
            where T_CONCRETE : T_INTERFACE
        {
            System.Type concrete_type = typeof(T_CONCRETE);
            System.Type interface_type = typeof(T_INTERFACE);
            System.Type user_type = typeof(T_USER);

            XApp.ASSERT(() => interface_type.IsInterface, XAppFailure.IOC_BINDING_FAILURE, "The bound type was not an interface: " + interface_type.ToString());
            XApp.ASSERT(() => !concrete_type.IsInterface && !concrete_type.IsAbstract, XAppFailure.IOC_BINDING_FAILURE, "Cannot bind type " + concrete_type.ToString() + " because it's not a concrete type");


            if (_attributes.ContainsKey(interface_type))
            {
                BindingAttributes attrs = _attributes[interface_type];
                attrs.UserMappings.Add(user_type, concrete_type);
            }
            else
            {
                _attributes.Add(interface_type, new BindingAttributes()
                {
                    IsSingleton = false,
                    UserMappings = new Dictionary<Type, Type>()
                    {
                        { user_type, concrete_type }
                    }
                });
            }
        }

        public void SetBindingSingleton<T_INTERFACE, T_CONCRETE>(params object[] parameters)
            where T_CONCRETE : T_INTERFACE
            where T_INTERFACE : IIoCSingleton
        {
            System.Type concrete_type = typeof(T_CONCRETE);
            System.Type interface_type = typeof(T_INTERFACE);

            XApp.ASSERT(() => interface_type.IsInterface, XAppFailure.IOC_BINDING_FAILURE, "The bound type was not an interface: " + interface_type.ToString());
            XApp.ASSERT(() => !concrete_type.IsInterface && !concrete_type.IsAbstract, XAppFailure.IOC_BINDING_FAILURE, "Cannot bind type " + concrete_type.ToString() + " because it's not a concrete type");

            List<System.Type> types = new List<Type>();
            types.Add(concrete_type);
            BindingAttributes attrs = new BindingAttributes()
            {
                IsSingleton = true,
                UserMappings = new Dictionary<Type,Type>(){ 
                    { interface_type, concrete_type }
                }
            };
            types.AddRange(parameters.Select(x => x.GetType()).ToArray());

            if (_attributes.ContainsKey(interface_type))
            {
                _attributes[interface_type] = attrs;
                attrs.SingletonObject = CreateObject<T_INTERFACE>(parameters);
            }
            else
            {
                _attributes.Add(interface_type, attrs);
                attrs.SingletonObject = CreateObject<T_INTERFACE>(parameters);
            }
        }

        T CreateObject<T>(params object[] parameters)
        {
            System.Type t_type = typeof(T);

            XApp.ASSERT(() => _attributes.ContainsKey(t_type), XAppFailure.IOC_BINDING_FAILURE, "No binding was found for " + t_type.ToString());
            XApp.ASSERT(() => _attributes[t_type].UserMappings.ContainsKey(t_type), XAppFailure.IOC_BINDING_FAILURE, "Trying to create a default binding, but none exists for " + t_type.ToString());

            System.Type bound_concrete_type = _attributes[t_type].UserMappings[t_type];

            XApp.ASSERT(() => bound_concrete_type != null, XAppFailure.IOC_BINDING_FAILURE, "No bound concrete type was found for " + t_type.ToString());

            ConstructorInfo ctor = bound_concrete_type.GetConstructor(parameters.Select(x => x.GetType()).ToArray());

            XApp.ASSERT (() => ctor != null, XAppFailure.IOC_BINDING_FAILURE, 
                "Bound concrete type has no constructor matching the provided types: " + bound_concrete_type.ToString());

            object concrete_type = ctor.Invoke(parameters);

            return (T)concrete_type;
        }

        T CreateMappedObject<T_MAP, T>(params object[] parameters)
        {
            System.Type t_type = typeof(T);
            System.Type user_type = typeof(T_MAP);

            XApp.ASSERT(() => _attributes.ContainsKey(t_type), XAppFailure.IOC_BINDING_FAILURE, "No binding was found for " + t_type.ToString());
            XApp.ASSERT(() => _attributes[t_type].UserMappings.ContainsKey(user_type), XAppFailure.IOC_BINDING_FAILURE, "User type " + user_type + " has not been mapped to interface type " + t_type.ToString());

            System.Type bound_concrete_type = _attributes[t_type].UserMappings[user_type];

            XApp.ASSERT(() => bound_concrete_type != null, XAppFailure.IOC_BINDING_FAILURE, "No bound concrete type was found for " + t_type.ToString());

            ConstructorInfo ctor = bound_concrete_type.GetConstructor(parameters.Select(x => x.GetType()).ToArray());

            XApp.ASSERT(() => ctor != null, XAppFailure.IOC_BINDING_FAILURE,
                "Bound concrete type has no constructor matching the provided types: " + bound_concrete_type.ToString());

            object concrete_type = ctor.Invoke(parameters);

            return (T)concrete_type;
        }

        void CheckTypeIsInstanced(System.Type t_type)
        {
            BindingAttributes attributes = _attributes[t_type];

            XApp.ASSERT(() => !attributes.IsSingleton, XAppFailure.IOC_BINDING_FAILURE,
                String.Format("The specified type ({0}) was bound as a singleton and cannot have an instance created.", t_type.ToString()));
        }

        void CheckTypeIsMapped(System.Type t_type)
        {
            XApp.ASSERT(() => _attributes.ContainsKey(t_type), XAppFailure.IOC_BINDING_FAILURE,
                "No binding exists for type:" + t_type.ToString());

        }

        public T CreateFor<T_USER_TYPE, T>()
        {
            System.Type t_type = typeof(T);

            CheckTypeIsMapped(t_type);
            CheckTypeIsInstanced(t_type);

            // Not a singleton, return a new instance
            return CreateMappedObject<T_USER_TYPE, T>();
        }

        public T CreateFor<T_USER_TYPE, T, T_PARAM1>(T_PARAM1 p1)
        {
            System.Type t_type = typeof(T);

            CheckTypeIsMapped(t_type);
            CheckTypeIsInstanced(t_type);

            // Not a singleton, return a new instance
            return CreateMappedObject<T_USER_TYPE, T>(new object[] { p1 });
        }

        public T CreateFor<T_USER_TYPE, T, T_PARAM1, T_PARAM2>(T_PARAM1 p1, T_PARAM2 p2)
        {
            System.Type t_type = typeof(T);

            CheckTypeIsMapped(t_type);
            CheckTypeIsInstanced(t_type);

            // Not a singleton, return a new instance
            return CreateMappedObject<T_USER_TYPE, T>(new object[] { p1, p2 });
        }

        public T CreateFor<T_USER_TYPE, T, T_PARAM1, T_PARAM2, T_PARAM3>(T_PARAM1 p1, T_PARAM2 p2, T_PARAM3 p3)
        {
            System.Type t_type = typeof(T);

            CheckTypeIsMapped(t_type);
            CheckTypeIsInstanced(t_type);

            // Not a singleton, return a new instance
            return CreateMappedObject<T_USER_TYPE, T>(new object[] { p1, p2, p3 });
        }

        public T Create<T>()
        {
            System.Type t_type = typeof(T);

            XApp.ASSERT(() => _attributes.ContainsKey(t_type), XAppFailure.IOC_BINDING_FAILURE, "No binding exists for type:" + t_type.ToString());

            BindingAttributes attributes = _attributes[t_type];

            XApp.ASSERT(() => !attributes.IsSingleton, XAppFailure.IOC_BINDING_FAILURE, String.Format("The specified type ({0}) was bound as a singleton and cannot have an instance created.", t_type.ToString()));

            // Not a singleton, return a new instance
            return CreateObject<T>();
        }

        public T GetSingleton<T>()
        {
            System.Type t_type = typeof(T);

            XApp.ASSERT(() => _attributes.ContainsKey(t_type), XAppFailure.IOC_BINDING_FAILURE, "No binding exists for type:" + t_type.ToString());

            BindingAttributes attributes = _attributes[t_type];

            XApp.ASSERT(() => attributes.IsSingleton, XAppFailure.IOC_BINDING_FAILURE, String.Format("The specified type ({0}) was not bound as a singleton and can only be created as a reference type.", t_type.ToString()));

            return (T)attributes.SingletonObject;
        }

        public T Create<T, T_PARAM1>(T_PARAM1 p1)
        {
            System.Type t_type = typeof(T);

            XApp.ASSERT(() => _attributes.ContainsKey(t_type), XAppFailure.IOC_BINDING_FAILURE);

            BindingAttributes attributes = _attributes[t_type];

            XApp.ASSERT(() => !attributes.IsSingleton, XAppFailure.IOC_BINDING_FAILURE, "Cannot pass parameters to a singleton type " + t_type.ToString());

            return CreateObject<T>(new object[] { p1 });
        }

        public T Create<T, T_PARAM1, T_PARAM2>(T_PARAM1 p1, T_PARAM2 p2)
        {
            System.Type t_type = typeof(T);

            XApp.ASSERT(() => _attributes.ContainsKey(t_type), XAppFailure.IOC_BINDING_FAILURE);

            BindingAttributes attributes = _attributes[t_type];

            XApp.ASSERT(() => !attributes.IsSingleton, XAppFailure.IOC_BINDING_FAILURE, "Cannot pass parameters to a singleton type" + t_type.ToString());

            return CreateObject<T>(new object[] { p1, p2 });
        }

        public T Create<T, T_PARAM1, T_PARAM2, T_PARAM3>(T_PARAM1 p1, T_PARAM2 p2, T_PARAM3 p3)
        {
            System.Type t_type = typeof(T);
            BindingAttributes attributes = _attributes[t_type];

            XApp.ASSERT(() => !attributes.IsSingleton, XAppFailure.IOC_BINDING_FAILURE, "Cannot pass parameters to a singleton type" + t_type.ToString());

            return CreateObject<T>(new object[] { p1, p2, p3 });
        }
    }
}
