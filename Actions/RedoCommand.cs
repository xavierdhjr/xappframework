﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace XAppFramework.Actions
{
    public class RedoCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return ActionSystem.Get.CanRedo();
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            ActionSystem.Get.Redo();
        }
    }
}
