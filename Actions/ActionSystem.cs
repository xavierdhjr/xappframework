﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace XAppFramework.Actions
{
    public class ActionSystem
    {
        class RecordedAction
        {
            public object[] Arguments;
            public IAction Action;

            public RecordedAction(IAction action, object[] args)
            {
                Arguments = args;
                Action = action;
            }

            public void Execute()
            {
                Action.Execute(Arguments);
            }

            public void Revert()
            {
                Action.Revert(Arguments);
            }
        }

#if DT_TESTING
        public static void DestroyInstance()
        {
            _system = null;
        }
#endif

        public static ActionSystem Get
        {
            get
            {
                if(_system == null)
                {
                    _system = new ActionSystem();
                }

                return _system;
            }
        }

        public static void Do<T>(params object[] args)
            where T : class, IAction, new()
        {
            Get._do<T>(args);
        }

        public static void Do(IAction action, params object[] args)
        {
            Get._do(action, args);
        }

        static ActionSystem _system;

        Stack<RecordedAction> _recordedActionStack;
        Stack<RecordedAction> _revertedActionStack;

        private ActionSystem()
        {
            _recordedActionStack = new Stack<RecordedAction>();
            _revertedActionStack = new Stack<RecordedAction>();
        }

        public bool CanUndo()
        {
            return _recordedActionStack.Count > 0;
        }

        public bool CanRedo()
        {
            return _revertedActionStack.Count > 0;
        }

        void _do<T>(params object[] args) 
            where T: class, IAction, new()
        {
            T action = new T();

            _do(action, args);
        }

        void _do(IAction action, params object[] args)
        {
            // Clear the revert history
            _revertedActionStack = new Stack<RecordedAction>();

            _recordedActionStack.Push(new RecordedAction(action, args));

            action.Execute(args);
        }

        public void Undo()
        {
            RecordedAction action = _recordedActionStack.Pop();
            action.Revert();

            _revertedActionStack.Push(action);
        }

        public void Redo()
        {
            RecordedAction action = _revertedActionStack.Pop();
            action.Execute();

            _recordedActionStack.Push(action);
        }

    }
}
