﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace XAppFramework.Actions
{
    public class ActionCommand : ICommand
    {
        IAction _action;
        object[] _args;

        public ActionCommand(ActionCommand other)
        {
            _action = other._action;
            _args = other._args;
        }

        public ActionCommand(IAction action, params object[] args)
        {
            _action = action;
            _args = args;
        }

        public bool CanExecute(object parameter)
        {
            return _action.CanExecute();
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            ActionSystem.Do(_action, _args);
        }
    }

}
