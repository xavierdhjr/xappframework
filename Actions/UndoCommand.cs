﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace XAppFramework.Actions
{

    public class UndoCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return ActionSystem.Get.CanUndo();
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            ActionSystem.Get.Undo();
        }
    }

}
