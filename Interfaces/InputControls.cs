﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XAppFramework.Reflection;

namespace XAppFramework
{
    public class InputLayoutAttrArgs
    {
        public TypeAssemblyInfo PopulateFromType;

        public List<string> FixedPopulateNames;
        public List<object> FixedPopulateValues;

        public object[] Attributes;
    }

    public delegate void OnValueUpdated(object value);
    public delegate void OnValueUpdated<T>(T value);

    public interface IInputLayout : IUIControl
    {
        event OnValueUpdated ValueChanged;

        void CreateLayout();
        bool IsEnabled { get; set; }
        bool IsDirty { get; }

        void SetValue(object o);
        object GetValue();

        void Focus();
    }

    public interface IGenericInputLayout : IInputLayout
    {
        string Name { get; }
        TypeAssemblyInfo Type { get; }
        bool IsExpanded { get; set; }

        bool HasValue();

        bool HasError();
        string GetErrorMessage();
        void FlagForError(string message);
        void ClearError();
    }

    public interface IBasicInputLayout : IInputLayout
    {

    }

    public interface IListInputLayout : IInputLayout
    {
        bool IsExpanded { get; set; }
    }

    public interface IObjectInputLayout : IInputLayout
    {
        bool IsExpanded { get; set; }
    }

    /// <summary>
    /// Represents a class that has a UI control associated with it
    /// </summary>
    public interface IUIControl
    {
        object GetControl();
    }

    /// <summary>
    /// Represents a class that can receive input and has a control for that input
    /// </summary>
    public interface IInputValueControl : IUIControl
    {
        bool IsDirty { get; }
        bool IsEnabled { get; set; }

        void Focus();
        void ClearValue();
        void SetValue(object o);
    }

    /// <summary>
    /// Represents a class that identifies what kind of input it needs to be based
    /// on the type of object given to it.
    /// </summary>
    public interface IGenericInputControl : IInputValueControl
    {
        string Name { get; }

        event OnValueUpdated ValueChanged;
        object GetValue();

        bool IsExpanded { get; set; }
    }

    /// <summary>
    /// Represents a class that has input of type T given to it.
    /// </summary>
    public interface IInputValueControl<T> : IInputValueControl
    {
        event OnValueUpdated<T> ValueChanged;

        T GetValue();
        void SetValue(T value);
    }

    /// <summary>
    /// Represents a class that should let the user choose an item from a list of values
    /// Like a dropdown menu.
    /// </summary>
    public interface IInputListControl : IInputValueControl<string>
    {
        int SelectedIndex { get; set; }

        void AddItem(string item);
    }

    public interface IDataQueryer<T_QUERY_TYPE, T_QUERY_DATA>
    {
        List<T_QUERY_DATA> FindAll(T_QUERY_TYPE query);
        T_QUERY_DATA FindFirst(T_QUERY_TYPE query);
    }

    public interface IPopulatedTypeQueryer : IDataQueryer<TypeAssemblyInfo, string>
    {

    }

    public interface IEnumQueryer : IDataQueryer<TypeAssemblyInfo, string>
    {
        int ParseEnum(string typeName, string value);
        string GetEnumName(string typeName, object value);
    }

    public interface IAssemblyQueryer : IDataQueryer<string, TypeAssemblyInfo>
    {
        string GetFieldName(FieldAssemblyInfo f);
    }

    public class AttributeQuery
    {
        public string FieldName;
        public string TypeName;
    }

    public interface IAttributeQueryer : IDataQueryer<AttributeQuery, TypeAssemblyInfo>
    {
    }

    /// <summary>
    /// Applies any attributes deemed necessary from fieldInfo onto layout
    /// </summary>
    public interface ILayoutAttributeApplier
    {
        void ApplyAttributes(FieldAssemblyInfo fieldInfo, IInputLayout layout);
    }

}
