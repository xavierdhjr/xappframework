﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAppFramework
{
    public interface IStyleProvider<T_STYLE_OBJ>
    {
        void ApplyStyle(T_STYLE_OBJ obj);
    }
}
