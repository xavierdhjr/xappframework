﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAppFramework
{
    public interface INotificationDialog
    {
        void ShowDialog(string title, string body);
    }

    public interface IYesNoDialog
    {
        void ShowDialog(string title, string body, System.Action on_yes, System.Action on_no);
    }

    public struct DialogParams
    {
        public string InitialDirectory;
        public string DefaultFileName;
        public string DefaultExtension;
        public string Filter;
    }

    public interface IOpenDialog
    {
        bool ShowOpenFileDialog(string initial_directory, System.Action<string> onFileSelected);
        bool ShowOpenFilesDialog(string initial_directory, System.Action<string[]> onFileSelected);

        bool ShowOpenFileDialog(DialogParams parameters, System.Action<string> onFileSelected);
        bool ShowOpenFilesDialog(DialogParams parameters, System.Action<string[]> onFilesSelected);
    }

    public interface INewFileDialog
    {
        bool ShowNewFileDialog(DialogParams parameters, System.Action<string> onFileCreated);
    }

    public interface ISaveFileDialog
    {
        bool ShowSaveFileDialog(DialogParams parameters, System.Action<string> onFileSaved);
    }
}
