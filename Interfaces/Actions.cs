﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAppFramework.Actions
{
    public interface IAction
    {
        bool CanExecute();
        void Execute(params object[] args);
        void Revert(params object[] args);
    }

    public interface IDeleteListItemAction : IAction
    {

    }

    public interface IMoveListItemAction : IAction
    {

    }
}
