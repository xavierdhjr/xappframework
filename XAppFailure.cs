﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAppFramework
{
    public class XAppFailure
    {
        public static XAppFailure IOC_BINDING_FAILURE = new XAppFailure("There was a problem creating or retrieving an application module.");
        public static XAppFailure UI_DATA_DISPLAY = new XAppFailure("The UI failed to display some data.");
        public static XAppFailure CACHE_ERROR = new XAppFailure("A caching error has occurred.");


        public string FailureMessage { get { return _failureMessage + "\n\t" + _bounsMessage; } }
        public uint FailureCode { get { return _failureCode; } }

        uint _failureCode;
        string _failureMessage;
        string _bounsMessage;

        static uint _failure_code = 0;

        public XAppFailure(string message)
        {
            _failureMessage = message;
            _failureCode = _failure_code++;
        }

        public XAppFailure WithMessage(string s)
        {
            _bounsMessage = s;
            return this;
        }

        public override string ToString()
        {
            return FailureMessage;
        }

        public static bool operator ==(XAppFailure a, XAppFailure b)
        {
            if (object.ReferenceEquals(a, null) && object.ReferenceEquals(b, null)) return true;
            if (object.ReferenceEquals(a, null) || object.ReferenceEquals(b, null)) return false;

            return a.FailureCode == b.FailureCode;
        }
        public static bool operator !=(XAppFailure a, XAppFailure b)
        {
            if (object.ReferenceEquals(a, null) && object.ReferenceEquals(b, null)) return false;
            if (object.ReferenceEquals(a, null) || object.ReferenceEquals(b, null)) return true;

            return a.FailureCode != b.FailureCode;
        }
    }
}
