﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections;

namespace XAppFramework.Reflection
{
    public class SerializedValueInfo
    {
        public string Name;
        public string Value;
    }

    [System.Serializable]
    public class FieldAssemblyInfo
    {
        public string Name;
        public TypeAssemblyInfo FieldType;
        public string DeclaringTypeName;
        public List<TypeAssemblyInfo> Attributes = new List<TypeAssemblyInfo>();

        public FieldAssemblyInfo(FieldInfo info)
        {
            Name = info.Name;
            FieldType = new TypeAssemblyInfo(info.FieldType);
            DeclaringTypeName = info.DeclaringType.FullName;

            foreach (System.Attribute a in info.GetCustomAttributes())
            {
                Attributes.Add(new TypeAssemblyInfo(a.GetType()));
            }
        }

#if DT_TESTING
        public FieldAssemblyInfo()
        {

        }
#endif

        public bool HasAttribute(System.Type t)
        {
            for(int i = 0; i < Attributes.Count; ++i)
            {
                if(t.FullName == Attributes[i].Name)
                {
                    return true;
                }
            }

            return false;
        }

        public bool HasAttribute<T>()
        {
            return HasAttribute(typeof(T));
        }
        public TypeAssemblyInfo GetAttribute<T>()
        {
            for(int i = 0; i < Attributes.Count; ++i)
            {
                if(Attributes[i].Name == typeof(T).ToString())
                {
                    return Attributes[i];
                }
            }

            return null;
        }

    }

}
