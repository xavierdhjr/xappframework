﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Reflection;

namespace XAppFramework.Reflection
{
    [Serializable]
    public class SerializableObject
    {
        public string Name;
        public bool IsNull;
        public bool IsList;
        public string Value;
        public TypeAssemblyInfo Type;

        public List<SerializableObject> Fields = new List<SerializableObject>();

        public static SerializableObject Serialize(object o, string name)
        {
            SerializableObject obj = new SerializableObject();

            if (o == null)
            {
                obj.IsNull = true;
                obj.Name = name;
                return obj;
            }

            obj.Type = new TypeAssemblyInfo(o.GetType());

            if (obj.Type.IsBasic)
            {
                obj.Value = o.ToString();
                obj.Name = name;
                return obj;
            }
            else if (o is System.Enum)
            {
                obj.Value = ((int)o).ToString();
                obj.Name = name;
                return obj;
            }
            else if (typeof(IEnumerable).IsAssignableFrom(o.GetType()))
            {
                obj.IsList = true;
                obj.Name = name;

                int idx = 0;
                foreach (var i in (IEnumerable)o)
                {
                    obj.Fields.Add(Serialize(i, obj.Name + "[" + idx + "]"));
                    idx++;
                }
                return obj;
            }
            else
            {
                System.Type type = o.GetType();

                FieldInfo[] infos = TypeAssemblyInfo.GetOrderedFields(type);

                obj.Name = name;

                for (int i = 0; i < infos.Length; ++i)
                {
                    obj.Fields.Add(Serialize(infos[i].GetValue(o), infos[i].Name));
                }
                return obj;
            }

        }
    }

}
