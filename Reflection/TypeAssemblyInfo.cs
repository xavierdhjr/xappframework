﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections;

namespace XAppFramework.Reflection
{
    [System.Serializable]
    public class TypeAssemblyInfo
    {
        public static TypeAssemblyInfo Null
        {
            get
            {
                return new TypeAssemblyInfo();
            }
        }

        static int depth = 0;

        public List<FieldAssemblyInfo> Fields = new List<FieldAssemblyInfo>();
        public bool IsEnum;
        public bool IsEnumerable;
        public bool IsArray;
        public bool IsBasic;
        public bool IsGeneric;
        public bool IsGenericTypeDefinition;
        public bool IsNullable;
        public string Name;
        public TypeAssemblyInfo ElementType;
        public TypeAssemblyInfo GenericTypeDefinition;
        public List<TypeAssemblyInfo> GenericArguments;

        public List<FieldAssemblyInfo> GetNonConstantFields()
        {
            List<FieldAssemblyInfo> fields = new List<FieldAssemblyInfo>();

            return fields;
        }
#if DT_TESTING
        public TypeAssemblyInfo() { }
#else
        TypeAssemblyInfo()
        {

        }
#endif

        public static FieldInfo[] GetOrderedFields(System.Type t)
        {
            List<FieldInfo> fields = new List<FieldInfo>();
            FieldInfo[] infos = t.GetFields();

            for (int i = 0; i < infos.Length; ++i)
            {
                // Ignore constant fields
                if (infos[i].IsLiteral)
                    continue;

                // Ignore static fields
                if (infos[i].IsStatic)
                    continue;

                fields.Add(infos[i]);
            }

            fields = fields.OrderBy(x => x.Name).OrderByDescending(x => x.FieldType.Name).ToList();

            int object_id_idx = fields.FindIndex(x => x.Name == "ObjectId");

            // Sort ObjectId to the front.
            if (object_id_idx != -1)
            {
                FieldInfo info = fields[object_id_idx];
                fields.RemoveAt(object_id_idx);
                fields.Insert(0, info);
            }

            return fields.ToArray();
        }

        public TypeAssemblyInfo(System.Type t)
        {
            depth++;

            if (depth > 20)
            {
                throw new System.InvalidOperationException("Recursive type detected.");
            }

            if (t == null)
            {
                depth--;
                return;
            }


            if (t.FullName == null) // This seems to occur with the generic type 'T'
            {
                depth--;
                return;
            }
                

            Name = t.FullName.ToString();
            IsEnumerable = t.IsArray || typeof(IEnumerable).IsAssignableFrom(t);
            IsArray = t.IsArray;
            IsNullable = Nullable.GetUnderlyingType(t) != null;
            IsBasic = IsBasicType(t);

            IsGeneric = t.IsGenericType;
            IsGenericTypeDefinition = t.IsGenericTypeDefinition;

            if (IsGeneric && !IsGenericTypeDefinition)
                GenericTypeDefinition = new TypeAssemblyInfo(t.GetGenericTypeDefinition());

            GenericArguments = new List<TypeAssemblyInfo>();
            foreach (System.Type generic_type in t.GetGenericArguments())
            {
                GenericArguments.Add(new TypeAssemblyInfo(generic_type));
            }

            ElementType = new TypeAssemblyInfo(t.GetElementType());
            FieldInfo[] fields = GetOrderedFields(t);

            Fields = fields.Select(x => new FieldAssemblyInfo(x)).ToList();

            IsEnum = t.IsEnum;

            depth--;
        }

        public static bool operator ==(TypeAssemblyInfo info, System.Type t)
        {
            if ((object)info == null && t == null)
                return true;

            return info.Name == t.FullName;
        }
        public static bool operator !=(TypeAssemblyInfo info, System.Type t)
        {
            

            if ((object)info != null && t == null)
                return true;
            if ((object)info == null && t != null)
                return true;
            if ((object)info == null && (object)t == null)
                return false;

            return info.Name != t.FullName.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj is TypeAssemblyInfo)
            {
                TypeAssemblyInfo a = (TypeAssemblyInfo)obj;
                return this == a;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }


        static bool IsBasicType(System.Type type)
        {
            System.Type inner_type = Nullable.GetUnderlyingType(type);

            if(inner_type != null)
            {
                type = inner_type;
            }

            if (type.IsPrimitive)
                return true;

            // Other 'lowest level' types for deserialization

            if (type == typeof(string))
                return true;

            if (type.IsEnum) 
                return true;

            return false;
        }

        public override string ToString()
        {
            return Name;
        }
    }

}
