﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace XAppFramework
{
    public static class XApp
    {
        public static void Dispose()
        {
            _lastFailure = null;
            _bindings = null;
        }

        static XAppFailure _lastFailure;

        public static XAppFailure GetLastFailure()
        {
            return _lastFailure;
        }

        public static void ClearLastFailure()
        {
            _lastFailure = null;
        }

        public const uint INVALID_HANDLE = 0;

        public static uint string_hash(string s)
        {
            uint h = 5381;
            for (uint i = 0; i < s.Length; ++i)
                h = ((h * 33) + s[(int)i]);

            return h;
        }

        /// <summary>
        /// Assert that the condition is true. If false, throws an exception.
        /// </summary>
        public static void ASSERT(System.Linq.Expressions.Expression<Func<bool>> condition, XAppFailure failure_type, string message = "")
        {
            Func<bool> expr = condition.Compile();

            if (!expr())
            {
                _lastFailure = failure_type;
                throw new Exception(failure_type.WithMessage(message).ToString());
            }
        }

        public static string GetRelativePath(string from, string to)
        {
            StringBuilder builder = new StringBuilder(1024);
            bool result = PathRelativePathTo(builder, from, 0, to, 0);
            return builder.ToString();
        }

        [System.Runtime.InteropServices.DllImport("shlwapi.dll", EntryPoint = "PathRelativePathTo")]
        static extern bool PathRelativePathTo(StringBuilder lpszDst,
            string from, UInt32 attrFrom,
            string to, UInt32 attrTo);

        /// <summary>
        /// Binds the concrete type T to the interface type I
        /// </summary>
        /// <typeparam name="I">The interface type. Must be an interface.</typeparam>
        /// <typeparam name="T">The concrete type.</typeparam>
        /// <param name="parameter_types">The parameters types that should be provided to the concrete type's constructor</param>
        public static void BindIoC<I, T>()
            where T : I
        {
            if (_bindings == null)
                _bindings = new IoCBindings();

            _bindings.SetBinding<I, T>();
        }

        /// <summary>
        /// Binds the concrete type T to the interface type I
        /// </summary>
        /// <typeparam name="I">The interface type. Must be an interface.</typeparam>
        /// <typeparam name="T">The concrete type.</typeparam>
        /// <param name="parameter_types">The parameters types that should be provided to the concrete type's constructor</param>
        public static void BindIoCMapped<I, T, T_USER_TYPE>()
            where T : I
        {
            if (_bindings == null)
                _bindings = new IoCBindings();

            _bindings.SetBinding<I, T, T_USER_TYPE>();
        }

        /// <summary>
        /// Binds the concrete type T to the interface type I as a singleton, creating a single instance that is shared across the application.
        /// </summary>
        /// <typeparam name="I">The interface type. Must be an interface.</typeparam>
        /// <typeparam name="T">The concrete type that will be made into a singleton.</typeparam>
        /// <param name="parameters">Parameters provided to the singleton's constructor</param>
        public static void BindIoC_Singleton<I, T>(params object[] parameters)
            where T : I
            where I : IIoCSingleton
        {
            if (_bindings == null)
                _bindings = new IoCBindings();

            _bindings.SetBindingSingleton<I, T>(parameters);
        }

        /// <summary>
        /// Creates an instance of the bound type with its default constructor.
        /// </summary>
        public static T Create<T>()
        {
            return _bindings.Create<T>();
        }

        /// <summary>
        /// Creates an instance of the bound type with its default constructor.
        /// </summary>
        public static T CreateFor<T_USER_TYPE, T>()
        {
            return _bindings.CreateFor<T_USER_TYPE, T>();
        }

        public static T CreateFor<T_USER_TYPE, T, T_PARAM1>(T_PARAM1 param1)
        {
            return _bindings.CreateFor<T_USER_TYPE, T, T_PARAM1>(param1);
        }

        public static T CreateFor<T_USER_TYPE, T, T_PARAM1, T_PARAM2>(T_PARAM1 param1, T_PARAM2 param2)
        {
            return _bindings.CreateFor<T_USER_TYPE, T, T_PARAM1, T_PARAM2>(param1, param2);
        }

        public static T CreateFor<T_USER_TYPE, T, T_PARAM1, T_PARAM2, T_PARAM3>(T_PARAM1 param1, T_PARAM2 param2, T_PARAM3 param3)
        {
            return _bindings.CreateFor<T_USER_TYPE, T, T_PARAM1, T_PARAM2, T_PARAM3>(param1, param2, param3);
        }

        /// <summary>
        /// Creates an instance of the bound type with the given constructor parameters.
        /// </summary>
        public static T Create<T, T_PARAM1>(T_PARAM1 param1)
        {
            return _bindings.Create<T, T_PARAM1>(param1);
        }

        /// <summary>
        /// Creates an instance of the bound type with the given constructor parameters.
        /// </summary>
        public static T Create<T, T_PARAM1, T_PARAM2>(T_PARAM1 param1, T_PARAM2 param2)
        {
            return _bindings.Create<T, T_PARAM1, T_PARAM2>(param1, param2);
        }

        /// <summary>
        /// Creates an instance of the bound type with the given constructor parameters.
        /// </summary>
        public static T Create<T, T_PARAM1, T_PARAM2, T_PARAM3>(T_PARAM1 param1, T_PARAM2 param2, T_PARAM3 param3)
        {
            return _bindings.Create<T, T_PARAM1, T_PARAM2, T_PARAM3>(param1, param2, param3);
        }

        /// <summary>
        /// Returns the singleton instance of the bound type.
        /// </summary>
        public static T GetSingleton<T>()
        {
            return _bindings.GetSingleton<T>();
        }


        static IoCBindings _bindings;
    }

}
